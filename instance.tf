resource "aws_instance" "jenkins" {
  ami           = var.AMIS[var.AWS_REGION]
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.jenkins-SG.id]
  key_name      = aws_key_pair.mykey.key_name
  user_data     = file("es-userdata.sh")

  tags = {
    Name  = "Jenkins"
  }
}
