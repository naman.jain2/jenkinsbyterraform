variable "AWS_REGION" {
  default = "us-east-1"
}

variable "AMIS" {
  type = map(string)
  default = {
    us-east-1 = "ami-0739f8cdb239fe9ae"
    us-west-2 = "ami-03d5c68bab01f3496"
    eu-west-1 = "ami-0a8e758f5e873d1c1"
  }
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "mykey.pub"
}

variable "INSTANCE_USERNAME" {
  default = "ubuntu"
}

