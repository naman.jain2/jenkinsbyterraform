output "jenkins_server_ip" {
  value = "http://${aws_instance.jenkins.public_ip}:8080"
}
